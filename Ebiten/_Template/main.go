package main

import (
	"fmt"
	"image/color"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

const (
	screenWidth  = 300
	screenHeight = 300
)

var (
	prevUpdateTime = time.Now()
)

type Game struct {
	pressedKeys []ebiten.Key
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight
}

func init() {
	var err error
	if err != nil {
		panic(err)
	}
}

func (g *Game) Update() error {
	//timeDelta := float64(time.Since(prevUpdateTime))
	prevUpdateTime = time.Now()

	g.pressedKeys = inpututil.AppendPressedKeys(g.pressedKeys[:0])

	for _, key := range g.pressedKeys {
		switch key.String() {
		case "ArrowDown":
			fmt.Println("ArrowDown")
		case "ArrowUp":
			fmt.Println("ArrowUp")
		case "ArrowRight":
			fmt.Println("ArrowRight")
		case "ArrowLeft":
			fmt.Println("ArrowLeft")
		}
	}

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.RGBA{0xff, 0, 0, 0xff})
	ebitenutil.DebugPrint(screen, "Hello, World!")
}

func main() {
	ebiten.SetWindowSize(screenWidth*2, screenHeight*2)
	ebiten.SetWindowTitle("The Nice Game")

	if err := ebiten.RunGame(&Game{}); err != nil {
		panic(err)
	}
}
